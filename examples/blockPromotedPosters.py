import time
from pydash.collections import find
from parler import Parler

mst = ''
jst = ''

client = Parler(mst, jst)

while True:

    results = client.getFeed()

    foundPromoters = False
    blockCount = 0

    for post in results['posts']:
        if 'sponsored' in post:
            foundPromoters = True
            creatorId = post['creator']
            creatorUser = find(results['users'], { 'id': creatorId })

            if creatorUser['blocked'] is False:
                client.block(creatorUser['username'])
                print(f'Blocked sponsored poster: {creatorUser["username"]}')
                blockCount += 1
                time.sleep(0.3)

    if foundPromoters:
        print(f'Blocked {str(blockCount)} accounts')
    else:
        print(f'Found no promoted posters to block')

    # sleep for 1 minutes
    print('Sleeping for 1 minute')
    time.sleep(60 * 1)
