import time
from parler import Parler

client = Parler(mst = '', jst = '')

results = client.getFeed()

for post in results['posts']:
    client.postVote(post['id'])
    print(f'Upvoted: {post["id"]}')
    time.sleep(0.3)
